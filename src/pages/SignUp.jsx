import { useState } from 'react';
import Layout from '../components/Layout';
import { Form, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { authSignUpCall } from '../APICall';
import { ToastContainer, toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';

function SignUp() {
  const history = useHistory();
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const submitHandler = async (e, firstName, lastName, email, username, password) => {
    e.preventDefault();
    await authSignUpCall({
      firstName,
      lastName,
      email,
      username,
      password,
    })
      .then(() => {
        toast.success('Register Success!');
        setTimeout(() => {
          history.push('/singin');
        }, 2000);
      })
      .catch(() => toast.error('Username already exist!'));
  };

  return (
    <Layout>
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <div
        className="d-flex justify-content-center align-items-center bg-dark"
        style={{ height: '85vh' }}
      >
        <Form onSubmit={(e) => submitHandler(e, firstName, lastName, email, username, password)}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label style={{ color: 'white' }}>First Name: </Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter first name"
              required
              onChange={(e) => setFirstName(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label style={{ color: 'white' }}>Last Name: </Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter last name"
              required
              onChange={(e) => setLastName(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label style={{ color: 'white' }}>Email: </Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              required
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label style={{ color: 'white' }}>Username: </Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter username"
              required
              onChange={(e) => setUsername(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label style={{ color: 'white' }}>Password: </Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              required
              onChange={(e) => setPassword(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Text style={{ color: 'white' }}>
              Already have an account?
              <span>
                <Link to="/singin">
                  <a className="text-primary"> Sign In</a>
                </Link>
              </span>
            </Form.Text>
          </Form.Group>
          <Button
            variant="primary"
            type="submit"
            onClick={(e) => submitHandler(e, firstName, lastName, email, username, password)}
          >
            Sign Up
          </Button>
        </Form>
      </div>
    </Layout>
  );
}

export default SignUp;

import { useEffect, useState } from 'react';
import Layout from '../components/Layout';
import { productGetAll, productCreateNew } from '../APICall';
import { ToastContainer, toast } from 'react-toastify';
import MenuCard from '../components/MenuCard';
import { Button, Modal, Form } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

function Product() {
  const history = useHistory();

  const [datas, setData] = useState([]);
  const [show, setShow] = useState(false);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [quantity, setQuantity] = useState('');

  const data = localStorage.getItem('accData') || null;
  const accRole = data != null ? JSON.parse(data).role : '1';

  useEffect(() => {
    async function fetchData() {
      await productGetAll()
        .then((res) => setData(res.data))
        .catch(() => toast.error('Problem happen when fetching products!'));
    }
    fetchData();
  }, []);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleSubmit = async (e, name, description, price, quantity) => {
    e.preventDefault();
    await productCreateNew({ name, description, price, quantity })
      .then(() => {
        toast.success('Succes creating new product');
        setTimeout(() => {
          history.go(0);
        }, 2000);
        history.push('/product');
      })
      .catch(() => toast.error('Failed creating new product'));
  };

  return (
    <Layout>
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <div className="d-flex bg-dark justify-content-end px-5">
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Creat New Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Name: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Product Name"
                  required
                  onChange={(e) => setName(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Description: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Product Description"
                  required
                  onChange={(e) => setDescription(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Price: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Product Price"
                  required
                  onChange={(e) => setPrice(Number(e.target.value))}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Quantity: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Product Quantity"
                  required
                  onChange={(e) => setQuantity(Number(e.target.value))}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button
              variant="primary"
              type="submit"
              onClick={(e) => handleSubmit(e, name, description, price, quantity)}
            >
              Create
            </Button>
          </Modal.Footer>
        </Modal>
        {accRole === '0' && (
          <Button variant="primary" onClick={handleShow}>
            Create Product
          </Button>
        )}
      </div>
      <div
        className="d-flex flex-wrap justify-content-center bg-dark"
        style={{ minHeight: '85vh' }}
      >
        {datas.map((data) => (
          <MenuCard
            key={data.id}
            id={data.id}
            name={data.name}
            description={data.description}
            price={data.price}
          />
        ))}
      </div>
    </Layout>
  );
}

export default Product;

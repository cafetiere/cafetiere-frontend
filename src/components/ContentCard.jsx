import { Card } from 'react-bootstrap';
import Post from '../assets/post.jpg';
import { Link } from 'react-router-dom';
import { FaComments } from 'react-icons/fa';

function ContentCard({ title, time, totalComment, ids, link }) {
  return (
    <Card
      style={{
        width: '18rem',
        marginRight: '50px',
        marginTop: '10px',
        cursor: 'pointer',
        height: '35vh',
      }}
    >
      {link !== '' ? <Card.Img variant="top" src={link} /> : <Card.Img variant="top" src={Post} />}

      <Card.Body>
        <Link to={`/post/${ids}`} style={{ color: 'black' }}>
          <Card.Title>{title}</Card.Title>
        </Link>
        <div className="d-flex justify-content-end">
          <FaComments />
          <Card.Text style={{ marginLeft: '15px' }}>{totalComment}</Card.Text>
        </div>
      </Card.Body>
      <Card.Footer>
        <Card.Text className="text-center">
          Published at: <span style={{ fontSize: '12px', color: 'blue' }}>{time}</span>
        </Card.Text>
      </Card.Footer>
    </Card>
  );
}

export default ContentCard;
